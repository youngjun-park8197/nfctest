/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
 
// NDEF Parser : NFC의 경우 데이터 교환 형식을 NDEF 메세지 형태로 기기 간 상호 교류
// 따라서, NdefMessageParser 클래스 생성 필요
// NdefMessage 파라메터 값을 받아서 static한 parser method를 노출시킴
// ParsedNdefRecord 객체의 리스트로써 NdefMessage의 내용을 return하고 NdefMessage 파라메터를 받도록 static한 parse method를 노출시킴


package com.example.android.nfc;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;

import com.example.android.nfc.record.ParsedNdefRecord;
import com.example.android.nfc.record.SmartPoster;
import com.example.android.nfc.record.TextRecord;
import com.example.android.nfc.record.UriRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for creating {@link ParsedNdefMessage}s.
 */
public class NdefMessageParser {

    // Utility class
    private NdefMessageParser() {

    }

    /** Parse an NdefMessage */
    public static List<ParsedNdefRecord> parse(NdefMessage message) {
        return getRecords(message.getRecords());
    }

    public static List<ParsedNdefRecord> getRecords(NdefRecord[] records) {
        List<ParsedNdefRecord> elements = new ArrayList<ParsedNdefRecord>();
        for (NdefRecord record : records) {
            if (UriRecord.isUri(record)) {
                elements.add(UriRecord.parse(record));
            } else if (TextRecord.isText(record)) {
                elements.add(TextRecord.parse(record));
            } else if (SmartPoster.isPoster(record)) {
                elements.add(SmartPoster.parse(record));
            }
        }
        return elements;
    }
}
